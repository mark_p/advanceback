import Vue from 'vue'
import Router from 'vue-router'
import VueValidator from 'vue-validator'
import Frame from './frame.vue'
import {readyConfig} from './config'
import RouteConfig from './route.config'

readyConfig(()=>{
    require('../../src/style/comm.scss')
    require('../../src/style/toast.scss')

    Vue.use(Router)
    Vue.use(VueValidator)

    const router = window.router = new Router();

    router.map(RouteConfig)

    router.redirect({
    '*': '/home'
    })

    Vue.config.debug = true;

    router.start(Frame, '#app-main');
})

